var algorithm;
function doThis() {
    document.getElementById("welcome").style.display = "none";
    document.getElementById("main").style.display = "block";
    var result = countTotal();
    // algorithm(result);
    displayResult(result);
}
async function fetchData() {
    // read our JSON
    let response = await fetch(localStorage.getItem('userurl') + ".json");
    let data = await response.json();
    return data;
}

async function loadUI() {
    let templateData = await fetchData();
    prepareHTML(templateData);
    $('#questionnaire').change(function () {
        var result = countTotal();
        displayResult(result);
    });

}

function countTotal() {
    var temp_algo=algorithm;
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    var counter = 0;
    $("*[data-common='true']").each(function (indiv) {
        var temp = $(this)[0].value;
        var temp_id= $(this)[0].id;
        if(temp_algo.includes(temp_id))
        {
            temp=(temp).replaceAll('"', '');
            temp_algo=temp_algo.replace(temp_id, temp);
        }
        /**************************************************************/
        counter = eval(temp_algo);
    });
    return counter;
}

function displayResult(result) {
    $.ajax({
        url: localStorage.getItem('userurl') + "/result/value.json",
        data: JSON.stringify(result),
        type: 'PUT',
        dataType: 'json',
        success: function (response) {
            console.log('done');
            $("#finalResult").text(result);
        },
        error: function (error) {
            console.log('error');
        }
    })
}


function prepareHTML(data) {
    Object.keys(data).map((key) => {
        if (key === "inputTypes") {
            fillInputType(data[key])
        }
        if (key === "welcome_screen") {
            fillSubHeading(data[key]["sub_heading"]);
            fillHeading(data[key]["heading"]);
            fillButtonText(data[key]["button_text"]);
        }
        if (key === "section") {
            fillQuestions(data[key]);
        }
        if (key === "counter") {
            counter = data[key];
        }
        if (key === "result")
        {
            algorithm = data[key].algo;
        }
    })
}
function fillQuestions(dataObject) {
    var dataObjectSize = Object.keys(dataObject).length;
    for (var i = 1; i < counter; i++) {
        if (dataObject[i]) {
            var question = dataObject[i]["title"];
            var type = dataObject[i]["type"];
            $("#questionnaire").append("<div class='form-group' id='" + i + "'></div>");
            $("#" + i).append("<label id='QUES_" + i + "' data-url='QUES_" + i + "'></label>");
            CHILD_URL["QUES_" + i] = URL["X"] + "/section/" + i + "/title";
            $("#QUES_" + i).append(question);
            if (type === "1") {
                $("#" + i).append("<select class='custom-select' id='INPUT_" + i + "' data-common='true'></select>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).find("select").append($('<option></option>').val(key).html(value));
                }
            }
            if (type === "2") {
                $("#" + i).append("<input type='number' class='form-control' data-common='true' data-url='INPUT_" + i + "' id='INPUT_" + i + "'>");
                CHILD_URL["INPUT_" + i] = URL["X"] + "/section/" + i + "/value";
                $("#" + i).find("input").attr("value", dataObject[i]["value"]);
            }
            if (type === "3") {
                $("#" + i).append("<br>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).append("<div class='form-check-inline'><input type='radio' data-common='true' id='INPUT_" + i + "' name= 'button-group' class='form-check-input'><label class='form-check-label' id='" + i + "RADIO_" + count + "' data-url='" + i + "RADIO_" + count + "'></label></div>");
                    $("#" + i).find("input").val(key);
                    $("#" + i + "RADIO_" + count).append(value);
                    CHILD_URL[i + "RADIO_" + count] = URL["X"] + "/section/" + i + "/values/" + '"' + count + '"';
                }
            }
            if (type === "4") {
                $("#" + i).append("<br>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).append("<div class='form-check'><input type='checkbox' data-common='true' id='INPUT_" + i + "' class='form-check-input'><label class='form-check-label' id='" + i + "CHECK_" + count + "' data-url='" + i + "CHECK_" + count + "'></label></div>");
                    $("#" + i).find("input").val(key);
                    $("#" + i + "CHECK_" + count).append(value);
                    CHILD_URL[i + "CHECK_" + count] = URL["X"] + "/section/" + i + "/values/" + '"' + count + '"';
                }
            }
        }
    }
}

function fillInputType(data) {
    var inputTypes = $('#selectBox');
    data.map((val, i) => {
        inputTypes.append(
            $('<option></option>').val(i).html(val)
        );
    });
}

function fillSubHeading(data) {
    var subHeading = $('#sub-heading');
    subHeading.append(data);
}

function fillHeading(data) {
    var subHeading = $('#heading');
    subHeading.append(data);
}

function fillButtonText(data) {
    var buttonText = $('#welcome-button');
    buttonText.append(data);
}

window.onload = loadUI();
function viewResult()
{
    $("#result-div").show();
}