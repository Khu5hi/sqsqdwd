window.onload = loadUI();
var templateData;
async function fetchData() {
    // read our JSON
    let response = await fetch(URL["X"] + ".json");
    let data = await response.json();
    return data;
}
async function loadUI() {
    templateData = await fetchData();
    prepareHTML(templateData);
}

function prepareHTML(data) {
  	console.log(data);
}

/**********************************************************/
async function createInstance()
{
	let response=await fetch(URL["Z"]+".json?shallow=true");
	let data= await response.json();
	return data;
}
async function createTemplate()
{
	let userData=await createInstance();
	var userkey=Object.keys(userData)[0];
	firebase.database().ref("users/" + userkey).push(templateData).then((snap) => 
	{
     const templatekey = snap.key;
     localStorage.setItem('userkey', userkey);
     localStorage.setItem('userurl', URL["Z"]+"/"+userkey+"/"+templatekey);
     localStorage.setItem('seturl', "/users/"+userkey+"/"+templatekey);
     window.open("TemplateBuilder.html","_parent"); 
  });
}
