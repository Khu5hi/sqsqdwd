/**----Global variable to access database counter for question count----**/
var counter;
/**----Global variable to keep account of questions----**/
var quesArr = [];
/**----Initial UI render on window load----**/
window.onload = loadUI();
async function loadUI() {
    let templateData = await fetchData();
    prepareHTML(templateData);
}
function prepareHTML(data) {
    Object.keys(data).map((key) => {
        if (key === "inputTypes") {
            fillInputType(data[key])
        }
        if (key === "welcome_screen") {
            fillHeading(data[key]["heading"]);
            fillSubHeading(data[key]["sub_heading"]);
            fillButtonText(data[key]["button_text"]);
        }
        if (key === "section") {
            fillQuestions(data[key]);
        }
        if (key === "counter") {
            counter = data[key];
        }
        if (key === "welcome") {
            fillWelcomePage(data[key]);
        }
    })
}
/**----Function to copy URL when going live----**/
async function fetchData() {
    // read our JSON
    USER_URL[localStorage.getItem('userkey')] = localStorage.getItem('userurl');
    let response = await fetch(localStorage.getItem('userurl') + ".json");
    let data = await response.json();
    return data;
}
/**----Function to fill questions----**/
function fillQuestions(dataObject) {
    var dataObjectSize = Object.keys(dataObject).length;
    for (var i = 1; i < counter; i++) {
        if (dataObject[i]) {
            quesArr.push(i);
            var question = dataObject[i]["title"];
            var type = dataObject[i]["type"];
            $("#questionnaire").append("<div class='form-group col-9 col-lg-9' id='" + i + "'></div>");
            $("#" + i).append("<label id='QUES_" + i + "' data-url='QUES_" + i + "' contenteditable='true'></label>");
            CHILD_URL["QUES_" + i] = USER_URL[localStorage.getItem('userkey')] + "/section/" + i + "/title";
            $("#QUES_" + i).append(question);
            if (type === "1") {
                $("#" + i).append("<select class='custom-select' data-common='true'></select>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).find("select").append($('<option></option>').val(key).html(value));
                }
            }
            if (type === "2") {
                $("#" + i).append("<input type='number' class='form-control' data-common='true' contenteditable='true' data-url='INPUT_" + i + "' id='INPUT_" + i + "'>");
                CHILD_URL["INPUT_" + i] = USER_URL[localStorage.getItem('userkey')] + "/section/" + i + "/value";
                $("#" + i).find("input").attr("value", dataObject[i]["value"]);
            }
            if (type === "3") {
                $("#" + i).append("<br>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).append("<div class='form-check-inline'><input type='radio' data-common='true' class='form-check-input'><label class='form-check-label' id='" + i + "RADIO_" + count + "' data-url='" + i + "RADIO_" + count + "' contenteditable='true'></label></div>");
                    $("#" + i).find("input").val(key);
                    $("#" + i + "RADIO_" + count).append(value);
                    CHILD_URL[i + "RADIO_" + count] = USER_URL[localStorage.getItem('userkey')] + "/section/" + i + "/values/" + '"' + count + '"';
                }
            }
            if (type === "4") {
                $("#" + i).append("<br>");
                var flag = 0;
                for (key in dataObject[i]["values"]) {
                    var value = dataObject[i]["values"][key];
                    var count = flag++;
                    $("#" + i).append("<div class='form-check'><input type='checkbox' data-common='true' class='form-check-input'><label class='form-check-label' id='" + i + "CHECK_" + count + "' data-url='" + i + "CHECK_" + count + "' contenteditable='true'></label></div>");
                    $("#" + i).find("input").val(key);
                    $("#" + i + "CHECK_" + count).append(value);
                    CHILD_URL[i + "CHECK_" + count] = USER_URL[localStorage.getItem('userkey')] + "/section/" + i + "/values/" + '"' + count + '"';
                }
            }
            $("#questionnaire").append("<div id='editPanel" + i + "' class='col-3 col-lg-3' style='text-align: center;padding-top: 15.5px;'></div>");
            $("#editPanel" + i + "").append("<div class='btn-group' id='editPanelButtonGroup" + i + "'></div>");
            $("#editPanelButtonGroup" + i + "").append("<button id='edit" + i + "' class='btn btn-primary a-btn-slide-text' onclick=editInput('" + i + "')><span class='fa fa-pencil' aria-hidden='true'></span></button>");
            $("#editPanelButtonGroup" + i + "").append("<button id='delete" + i + "' class='btn btn-primary a-btn-slide-text' onclick=deleteInput('" + i + "')><span class='fa fa-remove' aria-hidden='true'></span></button>");
            $("button").click(function(event){
                event.preventDefault();
            });
        }
    }

}
/**----Function to fill input types----**/
function fillInputType(data) {
    var inputTypes = $('#selectBox');
    data.map((val, i) => {
        inputTypes.append(
            $('<option></option>').val(i).html(val)
        );
    });
}

/**----Function to fill subheading----**/
function fillSubHeading(data) {
    $("#welcome-screen").append("<p class='lead' id='sub-heading' data-url='SUB_HEADING' contenteditable='true'></p>");
    $("#sub-heading").append(data);
    CHILD_URL["SUB_HEADING"] = USER_URL[localStorage.getItem('userkey')] + "/welcome_screen/sub_heading";
}

/**----Function to fill heading----**/
function fillHeading(data) {
    $("#welcome-screen").append("<h1 class='cover-heading' id='heading' data-url='HEADING' contenteditable='true'></h1>");
    $("#heading").append(data);
    CHILD_URL["HEADING"] = USER_URL[localStorage.getItem('userkey')] + "/welcome_screen/heading";


}

/**----Function to fill button text----**/
function fillButtonText(data) {
    $("#welcome-screen").append("<p class='lead'></p>").append("<button type='button' class='btn btn-danger' onclick='displayThis()' id='welcome-button' data-url='BUTTON_TEXT' contenteditable='true'></button>");
    $("#welcome-button").append(data);
    CHILD_URL["BUTTON_TEXT"] = USER_URL[localStorage.getItem('userkey')] + "/welcome_screen/button_text";
}

/**----Function to edit data----**/
function elemOper() {
    event.stopPropagation;
    var dataUrl = event.target.dataset["url"];
    var dbUrl = CHILD_URL[dataUrl] + ".json";
    var value = event.target.value ? event.target.value : event.target.textContent;
    saveData(dbUrl, value);
}

/**----Function to save data----**/
function saveData(dbUrl, value) {
    $.ajax({
        url: dbUrl,
        data: JSON.stringify(value),
        type: 'PUT',
        async: true,
        content: 'text/json',
        success: function(response) {
            console.log('done');
        },
        error: function(error) {
            console.log('error');
        }
    })
}

/**----Function to copy URL when going live----**/
function myFunction() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    document.execCommand("copy");

    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copied";
}

/**----Function to go live----**/
function goLive() {
    window.open("../Templates/LivePage.html");
}

/**----View the div to select input type for new question----**/
function viewSelectInput() {
    document.getElementById("select-input-type").style.display = "block";
    document.getElementById("viewSelectInputBtn").style.display = "none";
    document.getElementById("viewAlgorithmInputBtn").style.display = "inline-block";
    document.getElementById("formula-input").style.display = "none";
    document.getElementById("edit-input").style.display = "none";


}

/**----View the div to select input type for new algorithm----**/
function viewAlgorithmInput() {
    document.getElementById("formula-input").style.display = "block";
    document.getElementById("select-input-type").style.display = "none";
    document.getElementById("viewSelectInputBtn").style.display = "inline-block";
    document.getElementById("viewAlgorithmInputBtn").style.display = "none";
    document.getElementById("edit-input").style.display = "none";
    buildAlgorithm();
}

/**----Function to build algorithm----**/
function addToAlgo(id) {
    console.log(document.getElementById(id).value)
    document.getElementById('algorithmInput').value += (document.getElementById(id).value);
}
function buildAlgorithm()
{
    $('#formula-input').html('');
    for (var i = 0; i < quesArr.length; i++) {
        var index = quesArr[i];
        $("#formula-input").append("<button type='button' class='btn btn-info btn-circle btn-lg' value='INPUT_" + index + "'  id='INPUT_" + index + "' onclick=addToAlgo('INPUT_" + index + "')>" + (i + 1) + "</button>");
        console.log(index);
    }
    $("#formula-input").append("<div class='btn-group btn-matrix'><button type='button' class='btn btn-default' id='plus' value='+' onclick=addToAlgo('plus')>+</button><button type='button' class='btn btn-default' id='minus' value='-' onclick=addToAlgo('minus')>-</button><button type='button' class='btn btn-default' id='multiply' value='*' onclick=addToAlgo('multiply')>*</button><button type='button' class='btn btn-default' id='divide' value='/' onclick=addToAlgo('divide')>/</button><button type='button' class='btn btn-default' id='questionMark' value='?' onclick=addToAlgo('questionMark')>?</button><button type='button' class='btn btn-default' id='colon' value=':' onclick=addToAlgo('colon')>:</button><button type='button' class='btn btn-default' id='or' value='||' onclick=addToAlgo('or')>||</button><button type='button' class='btn btn-default' id='and' value='&&' onclick=addToAlgo('and')>&&</button><button type='button' id='modulus' value='%' class='btn btn-default' onclick=addToAlgo('modulus')>%</button></div>");
    $("#formula-input").append("<div class='form-group'><label for='comment'>Algorithm</label><textarea class='form-control' rows='5' id='algorithmInput'></textarea></div>");
    $("#formula-input").append("<button onclick='implementAlgorithm()' class='btn btn-primary'>CREATE</button>");
}
/**----Function to implement algorithm----**/
function implementAlgorithm()
{
    var algorithm=document.getElementById("algorithmInput").value;
    console.log(algorithm);
    firebase.database().ref(localStorage.getItem('seturl') + "/result/algo").set(algorithm);
    console.log("done");      
}

function editInput(id) {
    document.getElementById("select-input-type").style.display = "none";
    document.getElementById("viewSelectInputBtn").style.display = "inline-block";
    document.getElementById("viewAlgorithmInputBtn").style.display = "inline-block";
    document.getElementById("formula-input").style.display = "none";
    document.getElementById("edit-input").style.display = "block";

}

function deleteInput(id) {
    document.getElementById(id).parentNode.removeChild(document.getElementById(id));
    document.getElementById("editPanel"+id).parentNode.removeChild(document.getElementById("editPanel"+id));
    var path = firebase.database().ref(localStorage.getItem('seturl') + "/section/");
    path.child(id).remove();
    quesArr.splice( quesArr.indexOf(id), 1 );
    buildAlgorithm();
    console.log("done");
    console.log(quesArr);
}





var last_value = 0;

function changeFunc() {

    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    document.getElementById("addOptBtn").disabled = false;
    document.getElementById("removeOptBtn").disabled = false;
    document.getElementById("addQuesBtn").disabled = false;
    if (selectedValue == "1" || selectedValue == "3" || selectedValue == "4") {
        document.getElementById("group-input-type-container").style.display = "block";
        document.getElementById("single-input-type-container").style.display = "none";
        document.getElementById("addOptBtn").style.display = "inline-block";
        document.getElementById("removeOptBtn").style.display = "inline-block";
    } else {
        document.getElementById("group-input-type-container").style.display = "none";
        document.getElementById("single-input-type-container").style.display = "block";
        document.getElementById("addOptBtn").style.display = "none";
        document.getElementById("removeOptBtn").style.display = "none";
    }
}

function addOption() {
    if (document.getElementById("group-input-type-container").childElementCount == 1)
        last_value = 0;
    last_value += 1;
    var x = $("#edit-well-1").clone().addClass("new");
    x.find(".group-input-value").attr("value", last_value);
    x.find(".group-input-value").attr("id", "val" + last_value);
    x.find(".group-input-name").attr("id", "name" + last_value);
    $("#group-input-type-container").append(x);
}

function removeOption() {
    $(".new").remove();
}

function addQuestion() {
    /**----Global array that keeps an account of the div ids----**/
    quesArr.push(counter);
    /**----Main div----**/
    var parent = document.getElementById("questionnaire");
    /**----Question div----**/
    var div = document.createElement("DIV");
    div.setAttribute("class", "form-group");
    div.setAttribute("class", "col-9");
    div.setAttribute("class", "col-lg-9");
    div.setAttribute("id", counter);
    parent.appendChild(div);
    /**----Children of Question div----**/
    var label = document.createElement("LABEL");
    label.setAttribute("contenteditable", "true");
    label.setAttribute("data-url", "QUES_" + counter)
    label.setAttribute("id", "QUES_" + counter);
    CHILD_URL["QUES_" + counter] = URL["X"] + "/section/" + counter + "/title";
    var t = document.createTextNode("New Label");
    label.appendChild(t);
    div.appendChild(label);
    /**----Edit panel div----**/
    var editDiv=document.createElement("DIV");
    editDiv.setAttribute("id", "editPanel"+counter);
    editDiv.setAttribute("class", "col-3 col-lg-3");
    editDiv.setAttribute("style", "text-align: center;padding-top: 15.5px;");
    parent.appendChild(editDiv);
    /**----Children Edit panel div----**/
    var buttonGroup=document.createElement("DIV");
    buttonGroup.setAttribute("class", "btn-group");
    buttonGroup.setAttribute("id", "editPanelButtonGroup"+counter);
    editDiv.appendChild(buttonGroup);
    var editButton=document.createElement("BUTTON");
    editButton.setAttribute("class", "btn btn-primary a-btn-slide-text");
    editButton.setAttribute("id", "edit"+counter);
    editButton.setAttribute("onclick", "editInput("+counter+")");
    buttonGroup.appendChild(editButton);
    var editSpan=document.createElement("SPAN");
    editSpan.setAttribute("class", "fa fa-pencil");
    editSpan.setAttribute("aria-hidden", "true");
    editButton.appendChild(editSpan);
    var deleteButton=document.createElement("BUTTON");
    deleteButton.setAttribute("class", "btn btn-primary a-btn-slide-text");
    deleteButton.setAttribute("id", "delete"+counter);
    deleteButton.setAttribute("onclick", "deleteInput("+counter+")");
    buttonGroup.appendChild(deleteButton);
    var deleteSpan=document.createElement("SPAN");
    deleteSpan.setAttribute("class", "fa fa-remove");
    deleteSpan.setAttribute("aria-hidden", "true");
    deleteButton.appendChild(deleteSpan);
    /**----Get input type from select box----**/
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var count = document.getElementById("group-input-type-container").childElementCount;
    /**----Adding input types for each question based on type selected----**/
    var displayValues = [];
    var displayNames = [];
    if (selectedValue == "1") {
        var x = document.createElement("SELECT");
        x.setAttribute("class", "custom-select");
        x.setAttribute("data-common", true);
        div.appendChild(x);
        for (var i = 0; i < count; i++) {
            var z = document.createElement("option");
            z.setAttribute("value", document.getElementById("val" + i).value);
            z.setAttribute("data-common", true);
            var t = document.createTextNode(document.getElementById("name" + i).value);
            z.appendChild(t);
            x.appendChild(z);
            displayValues.push(document.getElementById("val" + i).value);
            displayNames.push(document.getElementById("name" + i).value)
        }
    } else if (selectedValue == "4") {
        for (var i = 0; i < count; i++) {
            var check = document.createElement("DIV");
            check.setAttribute("class", "form-check");
            div.appendChild(check);
            var x = document.createElement("INPUT");
            x.setAttribute("class", "form-check-input");
            x.setAttribute("type", "checkbox");
            x.setAttribute("value", document.getElementById("val" + i).value);
            x.setAttribute("data-common", true);
            var checklabel = document.createElement("LABEL");
            checklabel.setAttribute("class", "form-check-label");
            checklabel.setAttribute("contenteditable", "true");
            checklabel.setAttribute("id", counter + "CHECK_" + i);
            checklabel.setAttribute("data-url", counter + "CHECK_" + i);
            CHILD_URL[counter + "CHECK_" + i] = URL["X"] + "/section/" + counter + "/values/" + '"' + document.getElementById("val" + i).value + '"';
            var t = document.createTextNode(document.getElementById("name" + i).value);
            checklabel.appendChild(t);
            check.appendChild(x);
            check.appendChild(checklabel);
            displayValues.push(document.getElementById("val" + i).value);
            displayNames.push(document.getElementById("name" + i).value)
        }

    } else if (selectedValue == "3") {
        for (var i = 0; i < count; i++) {
            var radio = document.createElement("DIV");
            radio.setAttribute("class", "form-check");
            div.appendChild(radio);
            var x = document.createElement("INPUT");
            x.setAttribute("class", "form-check-input");
            x.setAttribute("type", "radio");
            x.setAttribute("value", document.getElementById("val" + i).value);
            x.setAttribute("data-common", true);
            var radiolabel = document.createElement("LABEL");
            radiolabel.setAttribute("class", "form-check-label");
            radiolabel.setAttribute("contenteditable", "true");
            radiolabel.setAttribute("id", counter + "RADIO_" + i);
            radiolabel.setAttribute("data-url", counter + "RADIO_" + i);
            CHILD_URL[counter + "RADIO_" + i] = URL["X"] + "/section/" + counter + "/values/" + '"' + document.getElementById("val" + i).value + '"';
            var t = document.createTextNode(document.getElementById("name" + i).value);
            radiolabel.appendChild(t);
            radio.appendChild(x);
            radio.appendChild(radiolabel);
            displayValues.push(document.getElementById("val" + i).value);
            displayNames.push(document.getElementById("name" + i).value)
        }
    } else {
        var x = document.createElement("INPUT");
        x.setAttribute("class", "form-control");
        x.setAttribute("type", "text");
        x.setAttribute("value", document.getElementById("default-value-text").value);
        x.setAttribute("placeholder", document.getElementById("placeholder-text").value);
        x.setAttribute("min", document.getElementById("min-value-text").value);
        x.setAttribute("max", document.getElementById("max-value-text").value);
        x.setAttribute("contenteditable", "true");
        x.setAttribute("id", "INPUT_" + counter);
        x.setAttribute("data-url", "INPUT_" + counter);
        x.setAttribute("data-common", true);
        CHILD_URL["INPUT_" + counter] = URL["X"] + "/section/" + counter + "/value";
        div.appendChild(x);
        var displayValue = document.getElementById("default-value-text").value;

    }
    /**----Adding the question and input into the database----**/
    if (selectedValue == 2) {
        var obj = {
            title: "New Label",
            type: selectedValue,
            value: displayValue
        };
        firebase.database().ref(localStorage.getItem('seturl') + "/section/" + counter).set(obj);
    } 
    else {
        var obj = {
            title: "New Label",
            type: selectedValue
        };
        firebase.database().ref(localStorage.getItem('seturl') + "/section/" + counter).set(obj);
        for (var temp = 0; temp < displayNames.length; temp++) {
            var key = displayValues[temp];
            var value = displayNames[temp];
            firebase.database().ref(localStorage.getItem('seturl') + "/section/" + counter + "/values/" + '"' + key + '"').set(value);
        }

    }
    counter += 1;
    $.ajax({
        url: localStorage.getItem('userurl') + "/counter.json",
        data: JSON.stringify(counter),
        type: 'PUT',
        content: 'text/json',
        dataType: 'json',
        success: function(response) {
            console.log('done');
        },
        error: function(error) {
            console.log('error');
        }
    });
     $("button").click(function(event){
                event.preventDefault();
            });
}

function goBack() {
    if (document.getElementById("result").style.display == "block") {
        document.getElementById("welcome").style.display = "none";
        document.getElementById("main").style.display = "block";
        document.getElementById("result").style.display = "none";
        return;
    }
    if (document.getElementById("main").style.display == "block") {
        document.getElementById("welcome").style.display = "block";
        document.getElementById("main").style.display = "none";
        document.getElementById("main").style.display = "none";
        return;
    }
}

function goForward() {
    if (document.getElementById("welcome").style.display == "block") {
        document.getElementById("welcome").style.display = "none";
        document.getElementById("main").style.display = "block";
        document.getElementById("result").style.display = "none";
        return;
    }
    if (document.getElementById("main").style.display == "block") {
        document.getElementById("welcome").style.display = "none";
        document.getElementById("main").style.display = "none";
        document.getElementById("result").style.display = "block";
        return;
    }

}